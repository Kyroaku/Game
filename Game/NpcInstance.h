#pragma once

#include "GameObject.h"
#include "Npc.h"
#include "Target.h"

class Target;

class NpcInstance : public GameObject
{
	Npc *npc;
	GuiText *name;
	Target *target;
	glm::vec2 dstPosition;
	Statistics statistics;

	void updateAnimation();

public:
	NpcInstance(ObjectManager *manager);
	virtual ~NpcInstance();

	virtual void setNpc(Npc *npc);
	virtual Npc *getNpc();
	virtual void setName(std::string name);
	virtual std::string getName();
	virtual void setTarget(Target *target);
	virtual Target *getTarget();
	virtual glm::vec2 getDstPosition();
	virtual void setDstPosition(glm::vec2 t);
	virtual Statistics &getStatistics();

	virtual void useSkill(int id);

	virtual void makeDamage(Statistics statistics);

	void onInput();
	void onUpdate(float dt);
	void onDraw();
};