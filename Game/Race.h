#pragma once

#include "Texture.h"

class RaceTexture
{
public:
	Texture *male;
	Texture *female;
};