#pragma once

#include <vector>
#include <algorithm>

#include "Macros.h"
#include "GameObject.h"

class GameObject;

class ObjectManager
{
	std::vector<GameObject*> objects;

public:
	ObjectManager();
	~ObjectManager();
	void addObject(GameObject *go);
	GameObject *getObject(int id);
	void onInput();
	void onUpdate(float dt);
	void onDraw();
};