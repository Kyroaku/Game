#pragma once

#include "Engine.h"

#include "Console.h"

/* renders border of each GuiLayout */
//#define _DEBUG_SHOW_LAYOUT_SIZE

/* renders border of each GameObject */
//#define _DEBUG_SHOW_GAMEOBJECT_SIZE

/* prints all fonts in assets->font */
//#define _DEBUG_PRINT_FONTS

/* moves specified vector with keyboard and prints it to console. */
//#define _DEBUG_POSITION_TEST

class Game : public Engine
{
	Game();
	Game(const Game &) {}
	Game &operator=(const Game &) {}
	~Game();
public:
	/* system */
	bool running;

	/* GUI */
	GuiLayout *currentDialog;
	GuiText *fpsText;
	float fps_timer = 0.0f;
	int fps_counter = 0;

	/* objects */
	ObjectManager *objectManager;

	Map *map;

	std::vector<Npc*>npcs;
	std::vector<Item*>items;
	std::vector<Skill*>skills;
	Player *player;

	int body_id = 0;
	int head_id = 0;
	int torso_id = 0;
	int legs_id = 0;

public:
	void Game::init();

	void onInput();
	void onUpdate(float dt);
	void onRender();

	static Game &get();

	bool isRunning();
	void quit();

	void setCurrentDialog(std::string path);
	GuiLayout *getCurrentDialog();
};