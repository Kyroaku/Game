#include "Game.h"

/* _TODO_ */
/*

=================== Game

### Statistic system
** hp (health points)
** strength (physical damage) [0-100]
** stamina (physical damage resistance) [0-1]
** intelligence (magic damage) [0-100]
** mentality (magic resistance) [0-1]
** agility (speed, attack speed, casting speed)

### Animation

### Skills
** Animations after using skill: CastSpell -> EndCastingSpell
** Character can't move while casting spell
** Basic attack is skill too
** Add statistics
- statiscs of skill is it's power
** Add attacker as a member
** Implement causing damage after hitting the enemy

### Damage system
** Show window with target's HP
** damage = ([attacker.strength/attakcer.intelligence] + attacker.weapon.power) * [target.stamina/target.mentality]

### Npc
- quest system
- shopping system
- add 'aggresive type' member (Aggresive, HalfAggresive, Passive)
- add statistics system
- add drop list (only for Monster type)

* refactor fps code

=================== GUI

* Update GuiText class
- implement GuiText::wrapToLayout() method

* Update GuiWindow class
- fix moving window by mouse

* Update GuiButton class
- add button text

* Update GuiLayout class
- fix methods names

*/

Game::Game()
{
	srand(SDL_GetTicks());

	/* system */
	running = true;
}

Game::~Game()
{
	delete GuiWindow::getDefaultButtonX();
	delete GuiWindow::getDefaultTitleText();

	delete map;
	delete objectManager;
}

void Game::init()
{
	/* GUI */
	int w, h;
	SDL_GetWindowSize(getSDLWindow(), &w, &h);

	gui.init(getRenderer());
	//gui.setMode(SGUI_MODE_SHOW_SIZE);
	gui.resize(glm::vec2(w, h));
	gui.addTexture("Data/gui_window.png");
	gui.addTexture("Data/gui_window_button_x.png");
	gui.addTexture("Data/gui_default_button01.png");
	gui.addTexture("Data/gui_default_button02.png");
	gui.addFont("Data/Fonts/ArialUnicodeMS.bff");
	currentDialog = 0;

	gui.registerFactory(0x01, new GuiLayoutFactory());
	gui.registerFactory(0x02, new GuiWindowFactory());
	gui.registerFactory(0x03, new GuiButtonFactory());
	gui.registerFactory(0x04, new GuiTextFactory());
	gui.registerFactory(0x05, new GuiRefButtonFactory());
	gui.registerFactory(0x06, new GuiEditBoxFactory());
	gui.registerFactory(0x07, new GuiTreeViewFactory());
	gui.registerFactory(0x11, new GuiShopDialogFactory());

#pragma region GuiDefaultSettings
	/* GuiShopDialog */
	GuiShopDialog::setDefaultSlotTexture(gui.getTexture("Data/slot.png"));
#pragma endregion

	fpsText = new GuiText(
		gui.getFont("Data/fonts/TektonProBoldCond.bff"),
		"Fps: 0",
		glm::vec2(-5.0f, 5.0f)
		);
	fpsText->setLabel("fps");
	fpsText->setColor(255, 255, 255);
	fpsText->setAlign(GuiText::AlignRight, GuiText::AlignTop);
	fpsText->setJustify(GuiText::JustifyLeft, GuiText::JustifyTop);
	//fpsText->setAdjustMode(eAdjustMode::Vertical);
	gui.addLayout(fpsText);

	gui.load("Data/Dialogs/window.gui");

	/* objects */
	objectManager = new ObjectManager();

	/* Create Map */
	map = new Map();
	map->createTemplate(50, 38);
	map->setTileset(assets->getTexture("Data/tiles01.png"));

	/* Create Npc data */
	npcs.resize(2);
	npcs[0] = new Npc("Test Merchant", ENpcTypes::eNpcMerchant);
	npcs[0]->setTexture(eSexMale, assets->getTexture("Data/Characters/body/male/light.png"));
	npcs[0]->setTexture(eSexFemale, assets->getTexture("Data/Characters/body/female/light.png"));
	npcs[0]->setDialog("Data/Dialogs/window");
	npcs[0]->getStatistics().set(100.0f, 10.0f, 0.2f, 10.0f, 0.2f, 1.0f);
	npcs[0]->setCloth(EClothes::eClothHelmet, 0);
	npcs[1] = new Npc("Human", ENpcTypes::eNpcMerchant);
	npcs[1]->setTexture(eSexMale, assets->getTexture("Data/Characters/body/male/skeleton.png"));
	npcs[1]->setTexture(eSexFemale, assets->getTexture("Data/Characters/body/male/skeleton.png"));
	npcs[1]->getStatistics().set(10.0f, 10.0f, 0.2f, 10.0f, 0.2f, 1.0f);
	npcs[1]->setCloth(EClothes::eClothHelmet, 0);

	/* Create Item data */
	items.resize(4);
	items[0] = new Item("Leather Hat", EItemType::eItemHelmet, 0);
	items[0]->setTextureMale(assets->getTexture("Data/Characters/head/male/leather_cap_male.png"));
	items[0]->setTextureFemale(assets->getTexture("Data/Characters/head/female/leather_cap_female.png"));
	items[1] = new Item("Leather Armor", EItemType::eItemBreastplate, 0);
	items[1]->setTextureMale(assets->getTexture("Data/Characters/torso/male/leather_chest_male.png"));
	items[1]->setTextureFemale(assets->getTexture("Data/Characters/torso/female/leather_chest_female.png"));
	items[2] = new Item("Leather Pants", EItemType::eItemPants, 0);
	items[2]->setTextureMale(assets->getTexture("Data/Characters/legs/male/magenta_pants_male.png"));
	items[2]->setTextureFemale(assets->getTexture("Data/Characters/legs/female/magenta_pants_female.png"));
	items[3] = new Item("Leather Shoes", EItemType::eItemShoes, 0);
	items[3]->setTextureMale(assets->getTexture("Data/Characters/feet/male/brown_shoes_male.png"));
	items[3]->setTextureFemale(assets->getTexture("Data/Characters/feet/female/brown_shoes_female.png"));

	/* Create skill data */
	skills.resize(2);
	skills[0] = new Skill(ESkillType::eSkillShoot, assets->getTexture("Data/Skills/tornado.png"), eAnimSkill1);
	skills[0]->getStatistics().set(0.0f, 0.0f, 0.0f, 10.0f, 0.0f, 0.0f);
	skills[0]->setSize(glm::vec2(128.0f));
	skills[0]->setPositionOffset(
		glm::vec2(0.0f, -16.0f),
		glm::vec2(43.0f, 7.0f),
		glm::vec2(0.0f, 16.0f),
		glm::vec2(-43.0f, 7.0f)
		);
	skills[1] = new Skill(ESkillType::eSkillHit, assets->getTexture("Data/Skills/firelion.png"), eAnimSkill1);
	skills[1]->getStatistics().set(0.0f, 0.0f, 0.0f, 10.0f, 0.0f, 0.0f);
	skills[1]->setSize(glm::vec2(64.0f));
	skills[1]->setPositionOffset(
		glm::vec2(0.0f, -16.0f),
		glm::vec2(43.0f, 7.0f),
		glm::vec2(0.0f, 16.0f),
		glm::vec2(-43.0f, 7.0f)
		);

	/* Create game ;) */
	NpcInstance *object = new NpcInstance(objectManager);
	object->setNpc(npcs[0]);
	object->setPosition(glm::vec2(100.0f));
	object->setDstPosition(object->getPosition());
	object->setSize(glm::vec2(128.0f));

	object = new NpcInstance(objectManager);
	object->setNpc(npcs[1]);
	object->setPosition(glm::vec2(200.0f));
	object->setDstPosition(object->getPosition());
	object->setSize(glm::vec2(64.0f));


	player = 0;
	player = new Player(objectManager);
	player->setNpc(new Npc("Player", ENpcTypes::eNpcPlayer));
	player->getNpc()->setTexture(eSexMale, game.assets->getTexture("Data/Characters/body/male/darkelf.png"));
	player->getNpc()->setTexture(eSexFemale, game.assets->getTexture("Data/Characters/body/female/darkelf.png"));
	player->getNpc()->setSex(eSexMale);
	player->getNpc()->setCloth(EClothes::eClothHelmet, 0);
	player->getNpc()->setCloth(EClothes::eClothBreastplate, 1);
	player->getNpc()->setCloth(EClothes::eClothPants, 2);
	player->getNpc()->setCloth(EClothes::eClothGloves, -1);
	player->getNpc()->setCloth(EClothes::eClothShoes, 3);
	player->getStatistics().set(100.0f, 10.0f, 0.2f, 10.0f, 0.2f, 1.0f);
	player->setPosition(glm::vec2(200.0f));
	player->setDstPosition(player->getPosition());
	player->setSize(glm::vec2(64.0f));
}

void Game::onInput()
{
	gui.onInput();

	objectManager->onInput();

	// Dev console.
#if defined(_DEBUG)
	if (input.isKeyDown(SDL_SCANCODE_HOME))
	{
		Console::AdminMenu();
	}
#endif

	// Position testing.
#if defined(_DEBUG) && defined(_DEBUG_POSITION_TEST)
#define _SPEED (1.0f)
#define _GET ( skills[0]->getPositionOffset(eDirUp) )
#define _SET(p) ( skills[0]->setPositionOffset(eDirUp, p) )
	if (input.isKey(SDL_SCANCODE_LEFT))
		_SET(_GET + glm::vec2(-1.0f, 0.0f)*_SPEED);
	if (input.isKey(SDL_SCANCODE_RIGHT))
		_SET(_GET + glm::vec2(1.0f, 0.0f)*_SPEED);
	if (input.isKey(SDL_SCANCODE_UP))
		_SET(_GET + glm::vec2(0.0f, -1.0f)*_SPEED);
	if (input.isKey(SDL_SCANCODE_DOWN))
		_SET(_GET + glm::vec2(0.0f, 1.0f)*_SPEED);
	printf("%f, %f\n", _GET.x, _GET.y);
#endif
}
void Game::onUpdate(float dt)
{
	/* update fps */
	if (fps_timer >= 1.0f)
	{
		char buffer[32];
		sprintf_s(buffer, 32, "Fps: %d", fps_counter);
		fpsText->setText(buffer);
		fps_timer -= 1.0f;
		fps_counter = 0;
	}
	fps_timer += dt;
	fps_counter++;

	/* update game */
	gui.onUpdate(dt);
	/*
		after updating gui, 'currentDialog' is not destroyed, but has 'destroyed' flag set,
		so we can set variable to 0.
		WARNING: currentDialog can be destroyed only in 'onInput' method !
		*/
	if (currentDialog)
	if (currentDialog->isRemoved()) {
		currentDialog->destroy();
		currentDialog = 0;
	}

	objectManager->onUpdate(dt);
}
void Game::onRender()
{
	/* set viewport size to screen size */
	getRenderer()->setViewport(vec2(0.0f), vec2(-1.0f));

	/* render map */
	map->onDraw();

	objectManager->onDraw();

	/* render GUI */
	gui.onDraw();
}

Game &Game::get()
{
	static Game instance;
	return instance;
}

bool Game::isRunning()
{
	return running;
}
void Game::quit()
{
	running = false;
}
void Game::setCurrentDialog(std::string path)
{
	GuiLayout *layout = gui.addLayout(gui.load(path));
	if (currentDialog) {
		layout->setPosition(currentDialog->getPosition());
		currentDialog->destroy();
	}
	currentDialog = layout;
}
GuiLayout *Game::getCurrentDialog()
{
	return currentDialog;
}