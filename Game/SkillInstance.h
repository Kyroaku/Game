#pragma once

#include "GameObject.h"
#include "Skill.h"
#include "NpcInstance.h"

class SkillInstance : public GameObject
{
	Skill *skill;
	NpcInstance *target, *attacker;

	void onUpdateSkillShoot(float dt);
	void onUpdateSkillHit(float dt);
	void onUpdateSkillHitShoot(float dt);

public:
	SkillInstance(ObjectManager *manager);
	SkillInstance(ObjectManager *manager, Skill *skill, NpcInstance *target, NpcInstance *attacker);
	~SkillInstance();

	void setSkill(Skill *skill);
	Skill *getSkill();
	void setTarget(NpcInstance *target);
	NpcInstance *getTarget();

	void onUpdate(float dt);
};