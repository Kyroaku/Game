#pragma once

#include "Macros.h"
#include "NpcInstance.h"
#include "SkillInstance.h"

class Player : public NpcInstance
{
	NpcInstance *selected;

public:
	Player(ObjectManager *manager);
	~Player();

	void setSelectedNpc(NpcInstance *npc);
	NpcInstance *getSelectedNpc();

	void onInput();
	void onUpdate(float dt);
	void onDraw();
};