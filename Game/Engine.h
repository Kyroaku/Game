#pragma once

#include <memory>
#include <SDL/SDL.h>
#include <glm\glm.hpp>
#include <unordered_map>
#include <conio.h>

#include "Assets.h"
#include "BFF_Font.h"

#include "GuiManager.h"
#include "GuiShopDialog.h"

#include "ObjectManager.h"
#include "Map.h"
#include "GameObject.h"
#include "Player.h"
#include "Npc.h"
#include "NpcInstance.h"
#include "Item.h"
#include "Skill.h"
#include "Macros.h"

using namespace glm;
using std::string;

class Engine
{
	SDL_Window *sdlWindow;
	SDL_Renderer *sdlRenderer;
	GuiRenderer *renderer;

public:
	Assets *assets;

	Engine();
	~Engine();

	GuiRenderer *getRenderer();
	SDL_Window *getSDLWindow();
	SDL_Renderer *getSDLRenderer();

	/* helpers */

	glm::vec2 getTextureSize(SDL_Texture *texture);

	/* collisions */
	bool isPointInRect(glm::vec2 p, glm::vec2 r1, glm::vec2 r2);
};