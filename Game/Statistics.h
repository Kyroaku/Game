#pragma once

class Statistics
{
public:
	float hp, strength, stamina, intelligence, mentality, agility;
public:
	Statistics(float hp, float strength, float stamina, float intelligence, float mentality, float agility);
	Statistics();
	void set(float hp, float strength, float stamina, float intelligence, float mentality, float agility);
};