#include "Player.h"

#include "Game.h"

Player::Player(ObjectManager *manager) :
NpcInstance(manager)
{
	
}
Player::~Player()
{

}

void Player::setSelectedNpc(NpcInstance *npc)
{
	this->selected = npc;
}
NpcInstance *Player::getSelectedNpc()
{
	return this->selected;
}

void Player::onInput()
{
	super::onInput();

	if (input.isButton(SDL_BUTTON_RIGHT)) {
		getTarget()->setAction(ETargetAction::eNone);
		setDstPosition(input.mouse);
	}

	if (getTarget()->getNpc())
	//if (getTarget()->getNpc()->getStatistics().hp > 0.0f)
	{
		if (input.isKeyDown(SDL_SCANCODE_1))
		{
			useSkill(0);
		}
		if (input.isKeyDown(SDL_SCANCODE_2))
		{
			useSkill(1);
		}
	}
}
void Player::onUpdate(float dt)
{
	super::onUpdate(dt);

	if (getTarget()->getNpc())
	if (getTarget()->getAction() == ETargetAction::eTalk)
	if (glm::length(getTarget()->getNpc()->getPosition() - getPosition()) < 30.0f)
	{
		game.setCurrentDialog(getTarget()->getNpc()->getNpc()->getDialogPath());
		getTarget()->setAction(ETargetAction::eGo);
	}
}

void Player::onDraw()
{
	super::onDraw();


}