#include "Texture.h"

Texture::Texture(GuiTexture *_texture, glm::ivec2 _segments)
: texture(_texture)
, segments(_segments)
{

}

void Texture::setAnimation(int _animation, unsigned int _start, unsigned int _end)
{
	animations[_animation].set(_start, _end);
}