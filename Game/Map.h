#pragma once

#include <stdio.h>

#include "Texture.h"
#include "Macros.h"

class Map
{
public:
	short *groundData;
	int width, height;
	Texture *tileset;

	Map();
	~Map();

	void setTileset(Texture *tileset);
	void load(const char *path);

	void onDraw();

	void createTemplate(int w, int h);
};