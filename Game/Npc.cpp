#include "Npc.h"
#include "Game.h"

Npc::Npc()
: name("Npc")
, type(ENpcTypes::eNpcMerchant)
, dialog("")
, sex(eSexMale)
{
	for (int i = 0; i < EClothes::eClothLast; i++)
		clothes[i] = -1;
}
Npc::Npc(std::string name, ENpcTypes type)
: Npc()
{
	texture[0] = texture[1] = 0;
	setName(name);
	setType(type);
	setDialog(dialog);
}
Npc::~Npc()
{

}

void Npc::setTexture(ENpcSex sex, Texture *texture)
{
	this->texture[sex] = texture;
}
Texture *Npc::getTexture()
{
	return texture[sex];
}
void Npc::setName(std::string name)
{
	this->name = name;
}
std::string Npc::getName()
{
	return this->name;
}
void Npc::setType(ENpcTypes type)
{
	this->type = type;
}
ENpcTypes Npc::getType()
{
	return type;
}
void Npc::setSex(ENpcSex sex)
{
	this->sex = sex;
}
ENpcSex Npc::getSex()
{
	return sex;
}
bool Npc::isTypeOf(ENpcTypes type)
{
	return (this->type == type);
}
void Npc::setCloth(int part, int id)
{
	clothes[part] = id;
}
int Npc::getCloth(int part)
{
	return clothes[part];
}
void Npc::setDialog(std::string dialog)
{
	this->dialog = dialog;
}
std::string Npc::getDialogPath()
{
	return dialog;
}
std::vector<DropItem> &Npc::getDropList()
{
	return drop;
}
Statistics &Npc::getStatistics()
{
	return statistics;
}