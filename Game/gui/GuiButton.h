#pragma once

#include "GuiText.h"

class GuiButton : public GuiLayout
{
	GuiTexture *texture;
	bool pressed, clicked;

	GuiText *text;

	static GuiTexture *defaultTexture;
	static GuiText *defaultText;

public:
	GuiButton(std::string text = defaultText->getText(), glm::vec2 p = glm::vec2(0.0f), glm::vec2 s = glm::vec2(-1.0f));
	GuiButton(GuiTexture *texture, std::string text = defaultText->getText(), glm::vec2 p = glm::vec2(0.0f), glm::vec2 s = glm::vec2(-1.0f));
	GuiButton(const GuiButton &c);

	virtual void onInput();
	virtual void onUpdate(float dt);
	virtual void onDraw();

	virtual void setTexture(GuiTexture *t);
	virtual void setText(GuiText *t);

	virtual GuiTexture *getTexture();
	virtual GuiText *getText();

	virtual bool isPressed();
	virtual bool isClicked();

	virtual void setSize(glm::vec2 s);

	virtual SerializedData Serialize();
	virtual void Deserialize(SerializedData &data);

	static void setDefaultTexture(GuiTexture *texture);
	static GuiTexture *getDefaultTexture();
	static void setDefaultText(GuiText *text);
	static GuiText *getDefaultText();
};