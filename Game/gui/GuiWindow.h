#pragma once

#include <string>

#include "GuiButton.h"
#include "GuiText.h"

class GuiButton;
class GuiText;

class GuiWindow : public GuiLayout
{
	GuiTexture *texture;
	GuiButton *buttonX;
	GuiText *title;
	GuiLayout *workspace;

	glm::vec2 mouseOffset;

	static GuiTexture *defaultTexture;
	static GuiButton *defaultButtonX;
	static GuiText *defaultTitleText;

public:
	GuiWindow(glm::vec2 p = glm::vec2(0.0f), glm::vec2 s = glm::vec2(-1.0f));
	GuiWindow(std::string t, glm::vec2 p = glm::vec2(0.0f), glm::vec2 s = glm::vec2(-1.0f));
	GuiWindow(const GuiWindow &c);
	~GuiWindow();
	virtual void onInput();
	virtual void onUpdate(float dt);
	virtual void onDraw();

	virtual void setSize(glm::vec2 s);

	virtual void setButtonX(GuiButton *button);
	virtual void setTitle(GuiText *text);

	virtual GuiButton *getButtonX();
	virtual GuiText *getTitle();
	virtual GuiLayout *getWorkspace();

	virtual GuiLayout *addToWorkspace(GuiLayout *layout);
	virtual void setWorkspace(glm::vec2 p, glm::vec2 s);

	virtual SerializedData Serialize();
	virtual void Deserialize(SerializedData &data);

	static void setDefaultTexture(GuiTexture *t);
	static void setDefaultButtonX(GuiButton *b);
	static void setDefaultTitleText(GuiText *t);
	static GuiTexture *getDefaultTexture();
	static GuiButton *getDefaultButtonX();
	static GuiText *getDefaultTitleText();
};