#include "GuiWindow.h"

#include "GuiManager.h"

GuiTexture *GuiWindow::defaultTexture = 0;
GuiButton *GuiWindow::defaultButtonX = 0;
GuiText *GuiWindow::defaultTitleText = 0;

GuiWindow::GuiWindow(glm::vec2 p, glm::vec2 s) :
GuiWindow(defaultTitleText->getText(), p, s)
{

}
GuiWindow::GuiWindow(std::string t, glm::vec2 p, glm::vec2 s) :
GuiLayout(p, s),
mouseOffset(glm::vec2(0.0f)),
texture(defaultTexture)
{
	setID("GuiWindow");

	setLabel("window");

	addLayout(buttonX = new GuiButton(*defaultButtonX));
	addLayout(title = new GuiText(*defaultTitleText));
	addLayout(workspace = new GuiLayout(p, s));

	buttonX->setBelongsToParent(true);
	buttonX->setLabel("windowbuttonx");

	title->setBelongsToParent(true);
	title->setText(t);
	title->setLabel("windowtitle");

	workspace->setBelongsToParent(true);
	workspace->setFocusable(false);
	workspace->setLabel("windowworkspace");
	workspace->setClipped(true);

	setWorkspace(glm::vec2(25.0f, 38.0f), glm::vec2(25.0f, 25.0f));

	if (getSize().x < 0.0f || getSize().y < 0.0f)
		setSize(gui.getRenderer()->getTextureSize(texture));
}
GuiWindow::GuiWindow(const GuiWindow &c)
: GuiLayout(c)
, texture(c.texture)
, mouseOffset(c.mouseOffset)
{
	buttonX = new GuiButton(*c.buttonX);
	title = new GuiText(*c.title);
	workspace = new GuiLayout(*c.workspace);
}
GuiWindow::~GuiWindow()
{

}

void GuiWindow::onInput()
{
	super::onInput();

	if (input.isButtonDown(SDL_BUTTON_LEFT))
	{
		mouseOffset = input.mouse - getPosition();
	}
}
void GuiWindow::onUpdate(float dt)
{
	super::onUpdate(dt);

	if (buttonX->isClicked())
		this->destroy();

	if (!input.isButton(SDL_BUTTON_LEFT))
		mouseOffset = glm::vec2(-1.0f);

	if (input.isButton(SDL_BUTTON_LEFT) && mouseOffset.x > 0.0f)
		setPosition(input.mouse - mouseOffset);
}
void GuiWindow::onDraw()
{
	super::onDraw();

	glm::vec2 position = glm::floor(getAbsolutePosition());

	glm::vec2 texSize = gui.getRenderer()->getTextureSize(texture);

	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(0.0f, 0.0f),
		(texSize / 3.0f),
		position,
		texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(1.0f, 0.0f),
		(texSize / 3.0f),
		position + (texSize / 3.0f) * glm::vec2(1.0f, 0.0f),
		glm::vec2(getSize().x - texSize.x*2.0f / 3.0f, texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(2.0f, 0.0f),
		(texSize / 3.0f),
		position + glm::vec2(getSize().x - texSize.x / 3.0f, 0.0f),
		texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(0.0f, 1.0f),
		(texSize / 3.0f),
		position + glm::vec2(0.0f, texSize.y / 3.0f),
		glm::vec2(texSize.x / 3.0f, getSize().y - 2.0f * texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(1.0f, 1.0f),
		(texSize / 3.0f),
		position + texSize / 3.0f,
		getSize() - 2.0f * texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(2.0f, 1.0f),
		(texSize / 3.0f),
		position + glm::vec2(getSize().x - texSize.x / 3.0f, texSize.y / 3.0f),
		glm::vec2(texSize.x / 3.0f, getSize().y - 2.0f * texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(0.0f, 2.0f),
		(texSize / 3.0f),
		position + glm::vec2(0.0f, getSize().y - texSize.y / 3.0f),
		texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(1.0f, 2.0f),
		(texSize / 3.0f),
		position + glm::vec2(texSize.x / 3.0f, getSize().y - texSize.y / 3.0f),
		glm::vec2(getSize().x - 2.0f * texSize.x / 3.0f, texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(2.0f, 2.0f),
		(texSize / 3.0f),
		position + getSize() - texSize / 3.0f,
		texSize / 3.0f);
}

void GuiWindow::setSize(glm::vec2 s)
{
	super::setSize(s);

	setWorkspace(glm::vec2(25.0f, 38.0f), glm::vec2(25.0f, 25.0f));
}

void GuiWindow::setButtonX(GuiButton *button)
{
	if (buttonX)
		delete buttonX;
	buttonX = button;
}
void GuiWindow::setTitle(GuiText *text)
{
	if (title)
		delete title;
	title = text;
}

GuiButton *GuiWindow::getButtonX()
{
	return buttonX;
}
GuiText *GuiWindow::getTitle()
{
	return title;
}

GuiLayout * GuiWindow::getWorkspace()
{
	return workspace;
}

GuiLayout * GuiWindow::addToWorkspace(GuiLayout * layout)
{
	return workspace->addLayout(layout);
}

void GuiWindow::setWorkspace(glm::vec2 p, glm::vec2 s)
{
	workspace->setPosition(p);
	workspace->setSize(getSize() - s - p);
}

SerializedData GuiWindow::Serialize()
{
	SerializedData data;
	data.Push(super::Serialize());
	data.Push(gui.getTexturePath(texture));
	data.Push(buttonX->Serialize());
	data.Push(title->Serialize());
	data.Push(workspace->Serialize());
	return data;
}

void GuiWindow::Deserialize(SerializedData & data)
{
	super::Deserialize(data);
	string str;
	data.Pop(str);
	texture = gui.getTexture(str);
	buttonX->Deserialize(data);
	title->Deserialize(data);
	workspace->Deserialize(data);
}

void GuiWindow::setDefaultTexture(GuiTexture *t)
{
	if (defaultTexture)
		;/* TODO: delete texture */
	defaultTexture = t;
}
void GuiWindow::setDefaultButtonX(GuiButton *b)
{
	if (defaultButtonX)
		delete defaultButtonX;
	defaultButtonX = b;
}
void GuiWindow::setDefaultTitleText(GuiText *t)
{
	if (defaultTitleText)
		delete defaultTitleText;
	defaultTitleText = t;
}
GuiTexture *GuiWindow::getDefaultTexture()
{
	return defaultTexture;
}
GuiButton *GuiWindow::getDefaultButtonX()
{
	return defaultButtonX;
}
GuiText *GuiWindow::getDefaultTitleText()
{
	return defaultTitleText;
}