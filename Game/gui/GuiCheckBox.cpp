#include "GuiCheckBox.h"

#include "GuiManager.h"

GuiButton *GuiCheckBox::defaultButtonCheck = 0;
GuiText *GuiCheckBox::defaultText = 0;
GuiTexture *GuiCheckBox::defaultTextureChecked = 0;
GuiTexture *GuiCheckBox::defaultTextureUnchecked = 0;

void GuiCheckBox::updateButtonCheck()
{
	if (checked)
		buttonCheck->setTexture(textureChecked);
	else
		buttonCheck->setTexture(textureUnchecked);
}

GuiCheckBox::GuiCheckBox()
	: GuiLayout()
	, checked(false)
	, textureChecked(defaultTextureChecked)
	, textureUnchecked(defaultTextureUnchecked)
{
	setID("GuiCheckBox");

	addLayout(buttonCheck = new GuiButton(*defaultButtonCheck));
	addLayout(text = new GuiText(*defaultText));

	buttonCheck->setFocusable(false);
	buttonCheck->setBelongsToParent(true);
	text->setFocusable(false);
	text->setBelongsToParent(true);

	setChecked(false);

	text->setPosition(glm::vec2(buttonCheck->getSize().x, 0.0f));
}

void GuiCheckBox::onInput()
{
	super::onInput();

	if (input.isButtonDown(SDL_BUTTON_LEFT)) {
		toggleChecked();
	}
}

void GuiCheckBox::onUpdate(float dt)
{
	super::onUpdate(dt);
}

void GuiCheckBox::onDraw()
{
	super::onDraw();
}

GuiButton * GuiCheckBox::getCheckButton()
{
	return buttonCheck;
}

GuiText * GuiCheckBox::getText()
{
	return text;
}

GuiTexture * GuiCheckBox::getTextureChecked()
{
	return textureChecked;
}

GuiTexture * GuiCheckBox::getTextureUnchecked()
{
	return textureUnchecked;
}

void GuiCheckBox::setTextureChecked(GuiTexture * t)
{
	textureChecked = t;
}

void GuiCheckBox::setTextureUnchecked(GuiTexture * t)
{
	textureUnchecked = t;
}

bool GuiCheckBox::isChecked()
{
	return checked;
}

void GuiCheckBox::setChecked(bool b)
{
	checked = b;
	updateButtonCheck();
}

void GuiCheckBox::toggleChecked()
{
	setChecked(checked ? false : true);
}

SerializedData GuiCheckBox::Serialize()
{
	SerializedData data;
	data.Push(super::Serialize());
	data.Push(buttonCheck->Serialize());
	data.Push(text->Serialize());
	data.Push(gui.getTexturePath(textureChecked));
	data.Push(gui.getTexturePath(textureUnchecked));
	return data;
}

void GuiCheckBox::Deserialize(SerializedData & data)
{
	super::Deserialize(data);
	buttonCheck->Deserialize(data);
	text->Deserialize(data);
	string str;
	data.Pop(str);
	textureChecked = gui.getTexture(str);
	data.Pop(str);
	textureUnchecked = gui.getTexture(str);
}

GuiButton * GuiCheckBox::getDefaultButtonCheck()
{
	return defaultButtonCheck;
}

GuiText * GuiCheckBox::getDefaultText()
{
	return defaultText;
}

GuiTexture * GuiCheckBox::getDefaultTextureChecked()
{
	return defaultTextureChecked;
}

GuiTexture * GuiCheckBox::getDefaultTextureUnchecked()
{
	return defaultTextureUnchecked;
}

void GuiCheckBox::setDefaultButtonCheck(GuiButton * button)
{
	defaultButtonCheck = button;
}

void GuiCheckBox::setDefaultText(GuiText * text)
{
	defaultText = text;
}

void GuiCheckBox::setDefaultTextureChecked(GuiTexture * texture)
{
	defaultTextureChecked = texture;
}

void GuiCheckBox::setDefaultTextureUnchecked(GuiTexture * texture)
{
	defaultTextureUnchecked = texture;
}
