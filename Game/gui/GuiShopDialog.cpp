#include "GuiShopDialog.h"

#include "GuiManager.h"

GuiTexture *GuiShopDialog::defaultSlotTexture = 0;

GuiShopDialog::GuiShopDialog(glm::vec2 p, glm::vec2 s) :
GuiShopDialog::GuiShopDialog(getDefaultTitleText()->getText(), p, s)
{

}
GuiShopDialog::GuiShopDialog(std::string t, glm::vec2 p, glm::vec2 s) :
GuiWindow(t, p, s),
slotTexture(defaultSlotTexture)
{
	setID("GuiShopDialog");
	addLayout(buttonBuy = new GuiButton("Buy", glm::vec2(-24.0f, -27.0f), glm::vec2(81.0f, 32.0f)));
	buttonBuy->setAlign(AlignRight, AlignBottom);
	buttonBuy->setBelongsToParent(true);
}
GuiShopDialog::~GuiShopDialog()
{

}

void GuiShopDialog::onDraw()
{
	super::onDraw();

	for (int y = 0; y < 7; y++)
	for (int x = 0; x < 5; x++)
		gui.getRenderer()->render(slotTexture, glm::vec2(getPosition().x + 25 + 33 * x, getPosition().y + 43 + 33 * y), glm::vec2(0.0f));
}

SerializedData GuiShopDialog::Serialize()
{
	SerializedData data;
	data.Push(super::Serialize());
	data.Push(gui.getTexturePath(slotTexture));
	data.Push(buttonBuy->Serialize());
	return data;
}

void GuiShopDialog::Deserialize(SerializedData & data)
{
	super::Deserialize(data);
	string str;
	data.Pop(str);
	slotTexture = gui.getTexture(str);
	buttonBuy->Deserialize(data);
}

void GuiShopDialog::setDefaultSlotTexture(GuiTexture *t)
{
	defaultSlotTexture = t;
}

GuiTexture *GuiShopDialog::getDefaultSlotTexture()
{
	return defaultSlotTexture;
}