#include "NpcInstance.h"

#include "Game.h"

NpcInstance::NpcInstance(ObjectManager *manager)
: GameObject(manager)
, npc(0)
{
	target = new Target();

	name = new GuiText();
	name->setFont(game.assets->font.TektonProBoldCond);
	name->setSize(getSize() + glm::vec2(0.0f, ((BFF_Font*)name->getFont())->getCellSize().y));
	name->setAlign(GuiText::AlignCenter, GuiText::AlignBottom);
	name->setText("NpcInstance");
}
NpcInstance::~NpcInstance()
{
	delete target;
	delete name;
}

void NpcInstance::setNpc(Npc *npc)
{
	this->npc = npc;
	name->setText(npc->getName());
	statistics = npc->getStatistics();
	getAnimation()->start();
}
Npc *NpcInstance::getNpc()
{
	return npc;
}
void NpcInstance::setName(std::string name)
{
	this->name->setText(name);
}
std::string NpcInstance::getName()
{
	return name->getText();
}
void NpcInstance::setTarget(Target *target)
{
	this->target = target;
}
Target *NpcInstance::getTarget()
{
	return target;
}
void NpcInstance::setDstPosition(glm::vec2 t)
{
	dstPosition = t;
}
glm::vec2 NpcInstance::getDstPosition()
{
	return dstPosition;
}
Statistics &NpcInstance::getStatistics()
{
	return statistics;
}

void NpcInstance::useSkill(int id)
{
	SkillInstance *skill = new SkillInstance(game.objectManager, game.skills[id], getTarget()->getNpc(), this);
	skill->setSize(game.skills[id]->getSize());
	skill->getAnimation()->setSpeed(3.0f);
	getAnimation()->set(game.skills[id]->getCastAnimation(), getTarget()->getNpc()->getPosition() - getPosition(), -1.0f, eAnimLoopOnce);
	getAnimation()->setFrame(0);
	skill->setPosition(getPosition() + skill->getSkill()->getPositionOffset(getAnimation()->getCurrentDirection()));
	setDstPosition(getPosition());
}

void NpcInstance::makeDamage(Statistics statistics)
{
	getStatistics().hp -= statistics.strength * getStatistics().stamina;
	getStatistics().hp -= statistics.intelligence * getStatistics().mentality;
}

void NpcInstance::updateAnimation()
{
	/* direction vector (NOT normalized) */
	glm::vec2 dir = getDstPosition() - getPosition();

	/* DEAD */
	if (statistics.hp <= 0.0f) {
		getAnimation()->set(eAnimDie, getAnimation()->getCurrentDirection());
		getAnimation()->setLoopType(eAnimLoopOnce);
		return;
	}

	if (getAnimation()->getCurrentAnimation() != eAnimDie)
	if (!getAnimation()->isAnimated())
	{
		getAnimation()->set(eAnimIdle, getAnimation()->getCurrentDirection(), -1.0f, eAnimLoopRepeat);
		getAnimation()->start();
	}

	if (getAnimation()->getCurrentAnimation() == eAnimIdle || getAnimation()->getCurrentAnimation() == eAnimWalk)
	{
		/* IDLE */
		if (dir == glm::vec2(0.0f))
			getAnimation()->set(eAnimIdle, getAnimation()->getCurrentDirection());
		/* WALK */
		else
		{
			getAnimation()->set(eAnimWalk, dir);
		}
	}
}

void NpcInstance::onInput()
{
	/* if player has clicked on that NPC */
	if (input.isButtonDown(SDL_BUTTON_LEFT) &&
		game.player != this &&
		game.isPointInRect(input.mouse, glm::vec2(getPosition().x - getSize().x / 2, getPosition().y - getSize().y), getSize()))
	{
		/* do action or set target */
		if (game.player->getTarget()->getNpc() == this)
		{
			if (getNpc())
			{
				if (getNpc()->isTypeOf(ENpcTypes::eNpcMerchant))
					game.player->getTarget()->set(this, ETargetAction::eTalk);
			}
		}
		else
			game.player->getTarget()->set(this);
	}
}
void NpcInstance::onUpdate(float dt)
{
	/* Set male or female texture */
	setTexture(getNpc()->getTexture());

	super::onUpdate(dt);

	if (getTarget()->getNpc())
	{
		if (getTarget()->isFollowed())
		{
			if (glm::length(getTarget()->getNpc()->getPosition() - getPosition()) > 30.0f)
				setDstPosition(getTarget()->getNpc()->getPosition());
			else
				setDstPosition(getPosition());
		}
	}

	updateAnimation();

	/* direction vector (NOT normalized) */
	glm::vec2 dir = getDstPosition() - getPosition();

	/* move player in the target's direction */
	if (getStatistics().hp > 0.0f)
	{
		if (glm::length(dir) > glm::length(glm::normalize(dir) * 60.f * dt))
		{
			if (getAnimation()->getCurrentAnimation() == eAnimWalk)
				move(glm::normalize(dir) * 60.f * dt);
		}
		else {
			setPosition(getDstPosition());
			if (getNpc()->getType() != ENpcTypes::eNpcPlayer)
				setDstPosition(glm::vec2(rand() % 600 + 100, rand() % 400 + 100));
		}
	}

	/* set color of name if selected */
	if (game.player)
	if (game.player->getTarget()->getNpc() == this)
		name->setColor(255, 255, 128);
	else
		name->setColor(255, 255, 255);
	/* set layout size */
	name->setPosition(glm::vec2(getPosition().x, getPosition().y - getSize().y));
}
void NpcInstance::onDraw()
{
	/* Draw npc */
	super::onDraw();

	if (!npc)
		return;

	int sex = npc->getSex();

	/* Draw clothes */
	for (int i = 0; i < EClothes::eClothLast; i++)
	if (npc->getCloth(i) >= 0)
	if (game.items[npc->getCloth(i)]->getTexture(sex))
	{
		int item_id = npc->getCloth(i);

		int texWidth, texHeight;
		SDL_QueryTexture((SDL_Texture*)game.items[item_id]->getTexture(sex)->texture, 0, 0, &texWidth, &texHeight);

		game.getRenderer()->render(
			game.items[item_id]->getTexture(sex)->texture,
			vec2((getAnimation()->getCurrentFrame() % game.items[item_id]->getTexture(sex)->segments.x) * texWidth / game.items[item_id]->getTexture(sex)->segments.x,
			(getAnimation()->getCurrentFrame() / game.items[item_id]->getTexture(sex)->segments.x) * texHeight / game.items[item_id]->getTexture(sex)->segments.y),
			ivec2(texWidth, texHeight) / game.items[item_id]->getTexture(sex)->segments,
			vec2(getPosition().x - getSize().x / 2, getPosition().y - getSize().y),
			getSize()
			);
	}

	/* Draw npc's name */
	name->onDraw();
}