#pragma once

#include <SDL/SDL.h>
#include <map>

#include "GuiRenderer.h"
#include "Macros.h"
#include "BFF_Font.h"
#include "Texture.h"

struct Assets
{
	GuiRenderer *renderer;

	struct Fonts
	{
		GuiFont
		*SketchFlowPrint,
		*ACaslonProRegular,
		*ArialUnicodeMS,
		*BrushScriptStd,
		*BuxtonSketch,
		*ComicSansMS,
		*SegoeScript,
		*TektonProBoldCond;
	};

	std::map<std::string, Texture*> textures;
	Fonts font;

	Assets(GuiRenderer *r)
	{
		renderer = r;
		Texture *texture = 0;

		SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "nearest");

		/* ====================================================================================================== Tiles */

		getTexture("Data/tiles01.png")->segments = glm::ivec2(9, 9);

		/* ================================================================================================== Characters  */

		/* MALE  */

		texture = getTexture("Data/Characters/body/male/light.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations[eAnimIdle + eDirUp].set(104, 104);
		texture->animations[eAnimIdle + eDirLeft].set(117, 117);
		texture->animations[eAnimIdle + eDirDown].set(130, 130);
		texture->animations[eAnimIdle + eDirRight].set(143, 143);
		texture->animations[eAnimWalk + eDirUp].set(105, 112);
		texture->animations[eAnimWalk + eDirLeft].set(118, 125);
		texture->animations[eAnimWalk + eDirDown].set(131, 138);
		texture->animations[eAnimWalk + eDirRight].set(144, 151);
		texture->animations[eAnimDie + eDirUp].set(260, 265);
		texture->animations[eAnimDie + eDirLeft].set(260, 265);
		texture->animations[eAnimDie + eDirDown].set(260, 265);
		texture->animations[eAnimDie + eDirRight].set(260, 265);
		texture->animations[eAnimSkill1 + eDirUp].set(0, 6);
		texture->animations[eAnimSkill1 + eDirLeft].set(13, 19);
		texture->animations[eAnimSkill1 + eDirDown].set(26, 32);
		texture->animations[eAnimSkill1 + eDirRight].set(39, 45);

		texture = getTexture("Data/Characters/body/male/dark.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/male/light.png")->animations;

		texture = getTexture("Data/Characters/body/male/dark2.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/male/light.png")->animations;

		texture = getTexture("Data/Characters/body/male/darkelf.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/male/light.png")->animations;

		texture = getTexture("Data/Characters/body/male/darkelf2.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/male/light.png")->animations;

		texture = getTexture("Data/Characters/body/male/orc.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/male/light.png")->animations;

		texture = getTexture("Data/Characters/body/male/red_orc.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/male/light.png")->animations;

		texture = getTexture("Data/Characters/body/male/tanned.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/male/light.png")->animations;

		texture = getTexture("Data/Characters/body/male/tanned2.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/male/light.png")->animations;

		texture = getTexture("Data/Characters/body/male/skeleton.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/male/light.png")->animations;

		/* FEMALE  */

		texture = getTexture("Data/Characters/body/female/light.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations[eAnimIdle + eDirUp].set(104, 104);
		texture->animations[eAnimIdle + eDirLeft].set(117, 117);
		texture->animations[eAnimIdle + eDirDown].set(130, 130);
		texture->animations[eAnimIdle + eDirRight].set(143, 143);
		texture->animations[eAnimWalk + eDirUp].set(105, 112);
		texture->animations[eAnimWalk + eDirLeft].set(118, 125);
		texture->animations[eAnimWalk + eDirDown].set(131, 138);
		texture->animations[eAnimWalk + eDirRight].set(144, 151);
		texture->animations[eAnimDie + eDirUp].set(260, 265);
		texture->animations[eAnimDie + eDirLeft].set(260, 265);
		texture->animations[eAnimDie + eDirDown].set(260, 265);
		texture->animations[eAnimDie + eDirRight].set(260, 265);
		texture->animations[eAnimSkill1 + eDirUp].set(0, 6);
		texture->animations[eAnimSkill1 + eDirLeft].set(13, 19);
		texture->animations[eAnimSkill1 + eDirDown].set(26, 32);
		texture->animations[eAnimSkill1 + eDirRight].set(39, 45);

		texture = getTexture("Data/Characters/body/female/dark.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/female/light.png")->animations;

		texture = getTexture("Data/Characters/body/female/dark2.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/female/light.png")->animations;

		texture = getTexture("Data/Characters/body/female/darkelf.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/female/light.png")->animations;

		texture = getTexture("Data/Characters/body/female/darkelf2.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/female/light.png")->animations;

		texture = getTexture("Data/Characters/body/female/orc.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/female/light.png")->animations;

		texture = getTexture("Data/Characters/body/female/red_orc.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/female/light.png")->animations;

		texture = getTexture("Data/Characters/body/female/tanned.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/female/light.png")->animations;

		texture = getTexture("Data/Characters/body/female/tanned2.png");
		texture->segments = glm::ivec2(13, 21);
		texture->animations = getTexture("Data/Characters/body/female/light.png")->animations;

		/* ===================================================================================================== Items */

		getTexture("Data/Characters/feet/male/black_shoes_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/feet/male/brown_shoes_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/feet/male/golden_boots_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/feet/male/maroon_shoes_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/feet/male/metal_boots_male.png")->segments = glm::ivec2(13, 21);

		getTexture("Data/Characters/feet/female/black.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/feet/female/black_shoes_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/feet/female/brown.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/feet/female/brown_longboots_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/feet/female/brown_shoes_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/feet/female/golden_boots_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/feet/female/gray.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/feet/female/maroon_longboots_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/feet/female/maroon_shoes_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/feet/female/metal_boots_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/feet/female/white.png")->segments = glm::ivec2(13, 21);

		getTexture("Data/Characters/gloves/male/golden_gloves_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/gloves/male/metal_gloves_male.png")->segments = glm::ivec2(13, 21);

		getTexture("Data/Characters/gloves/female/golden_gloves_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/gloves/female/metal_gloves_female.png")->segments = glm::ivec2(13, 21);

		getTexture("Data/Characters/head/male/chain_hood_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/male/chainhat_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/male/cloth_hood_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/male/golden_helm_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/male/leather_cap_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/male/metal_helm_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/male/red.png")->segments = glm::ivec2(13, 21);

		getTexture("Data/Characters/head/female/bronze_tiara.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/female/chain_hood_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/female/chainhat_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/female/cloth_hood_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/female/gold_tiara.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/female/golden_helm_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/female/iron_tiara.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/female/leather_cap_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/female/metal_helm_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/female/purple_tiara.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/female/red.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/head/female/silver_tiara.png")->segments = glm::ivec2(13, 21);

		getTexture("Data/Characters/legs/male/golden_greaves_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/legs/male/magenta_pants_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/legs/male/metal_pants_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/legs/male/red_pants_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/legs/male/robe_skirt_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/legs/male/teal_pants_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/legs/male/white_pants_male.png")->segments = glm::ivec2(13, 21);

		getTexture("Data/Characters/legs/female/golden_greaves_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/legs/female/magenta_pants_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/legs/female/metal_pants_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/legs/female/red_pants_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/legs/female/robe_skirt_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/legs/female/teal_pants_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/legs/female/white_pants_female.png")->segments = glm::ivec2(13, 21);

		getTexture("Data/Characters/torso/male/brown_longsleeve.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/male/gold_arms_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/male/gold_chest_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/male/jacket_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/male/leather_chest_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/male/leather_shoulders_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/male/mail_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/male/maroon_longsleeve.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/male/plate_arms_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/male/plate_chest_male.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/male/teal_longsleeve.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/male/white_longsleeve.png")->segments = glm::ivec2(13, 21);

		getTexture("Data/Characters/torso/female/blue_vest.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/brown_pirate.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/brown_sleeveless.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/brown_tunic.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/corset_black.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/corset_brown.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/corset_red.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/dress_w_sash_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/gold_arms_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/gold_chest_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/jacket_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/leather_chest_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/leather_shoulders_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/mail_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/maroon_pirate.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/maroon_sleeveless.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/maroon_tunic.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/overskirt.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/plate_arms_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/plate_chest_female.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/teal_pirate.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/teal_sleeveless.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/teal_tunic.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/tightdress_black.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/tightdress_lightblue.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/tightdress_red.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/tightdress_white.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/underdress.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/white_pirate.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/white_sleeveless.png")->segments = glm::ivec2(13, 21);
		getTexture("Data/Characters/torso/female/white_tunic.png")->segments = glm::ivec2(13, 21);

		/* ===================================================================================================== Skills */
		getTexture("Data/Skills/tornado.png")->segments = glm::ivec2(4, 4);
		getTexture("Data/Skills/tornado.png")->animations[eAnimIdle + eDirUp].set(0, 4);
		getTexture("Data/Skills/tornado.png")->animations[eAnimIdle + eDirLeft].set(0, 4);
		getTexture("Data/Skills/tornado.png")->animations[eAnimIdle + eDirDown].set(0, 4);
		getTexture("Data/Skills/tornado.png")->animations[eAnimIdle + eDirRight].set(0, 4);
		getTexture("Data/Skills/tornado.png")->animations[eAnimWalk + eDirUp].set(5, 10);
		getTexture("Data/Skills/tornado.png")->animations[eAnimWalk + eDirLeft].set(5, 10);
		getTexture("Data/Skills/tornado.png")->animations[eAnimWalk + eDirDown].set(5, 10);
		getTexture("Data/Skills/tornado.png")->animations[eAnimWalk + eDirRight].set(5, 10);
		getTexture("Data/Skills/tornado.png")->animations[eAnimDie + eDirUp].set(11, 15);
		getTexture("Data/Skills/tornado.png")->animations[eAnimDie + eDirLeft].set(11, 15);
		getTexture("Data/Skills/tornado.png")->animations[eAnimDie + eDirDown].set(11, 15);
		getTexture("Data/Skills/tornado.png")->animations[eAnimDie + eDirRight].set(11, 15);

		getTexture("Data/Skills/firelion.png")->segments = glm::ivec2(4, 16);
		getTexture("Data/Skills/firelion.png")->animations[eAnimIdle + eDirDown].set(0, 11);
		getTexture("Data/Skills/firelion.png")->animations[eAnimIdle + eDirLeft].set(16, 27);
		getTexture("Data/Skills/firelion.png")->animations[eAnimIdle + eDirUp].set(48, 59);
		getTexture("Data/Skills/firelion.png")->animations[eAnimIdle + eDirRight].set(32, 43);
		getTexture("Data/Skills/firelion.png")->animations[eAnimDie + eDirDown].set(12, 15);
		getTexture("Data/Skills/firelion.png")->animations[eAnimDie + eDirLeft].set(28, 31);
		getTexture("Data/Skills/firelion.png")->animations[eAnimDie + eDirUp].set(60, 63);
		getTexture("Data/Skills/firelion.png")->animations[eAnimDie + eDirRight].set(44, 47);

		/* ======================================================================================================= Fonts */

		font.SketchFlowPrint = r->loadFont("Data/Fonts/SketchFlowPrint.bff");
		font.ACaslonProRegular = r->loadFont("Data/Fonts/ACaslonProRegular.bff");
		font.ArialUnicodeMS = r->loadFont("Data/Fonts/ArialUnicodeMS.bff");
		font.BrushScriptStd = r->loadFont("Data/Fonts/BrushScriptStd.bff");
		font.BuxtonSketch = r->loadFont("Data/Fonts/BuxtonSketch.bff");
		font.ComicSansMS = r->loadFont("Data/Fonts/ComicSansMS.bff");
		font.SegoeScript = r->loadFont("Data/Fonts/SegoeScript.bff");
		font.TektonProBoldCond = r->loadFont("Data/Fonts/TektonProBoldCond2.bff");
	}
	~Assets()
	{
		/* TODO: delete assets */
	}

	Texture *getTexture(std::string t)
	{
		if (textures[t] == 0) {
			printf("%s\n", t.c_str());
			textures[t] = new Texture(renderer->loadTexture(t));
		}
		return textures[t];
	}
};