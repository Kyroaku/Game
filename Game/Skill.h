#pragma once

#include "Texture.h"
#include "Statistics.h"

enum ESkillType
{
	eSkillShoot,
	eSkillHit,
	eSkillHitShoot
};

class Skill
{
	ESkillType type;
	Texture *texture;
	Statistics statistics;
	EAnimations castAnimation;
	glm::vec2 positionOffset[4];
	glm::vec2 size;

public:
	Skill(ESkillType type, Texture *texture, EAnimations castAnimation);
	Skill();
	~Skill();

	void setType(ESkillType type);
	ESkillType getType();
	void setTexture(Texture *texture);
	Texture *getTexture();
	Statistics &getStatistics();
	void setCastAnimation(EAnimations castAnimation);
	EAnimations getCastAnimation();
	void setPositionOffset(EDirection dir, glm::vec2 p);
	void setPositionOffset(glm::vec2 up, glm::vec2 right, glm::vec2 down, glm::vec2 left);
	glm::vec2 getPositionOffset(EDirection dir);
	void setSize(glm::vec2 s);
	glm::vec2 getSize();
};