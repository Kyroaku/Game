#pragma once

#include <string>
#include <stdio.h>

#include "Npc.h"

using std::string;

class Console
{
public:
	static void AdminMenu();
	static void NpcMenu();
	static void ClothesMenu(int npc_id);
	static void SexMenu(int npc_id);
};