#include "SkillInstance.h"

SkillInstance::SkillInstance(ObjectManager *manager, Skill *skill, NpcInstance *target, NpcInstance *attacker)
: GameObject(manager)
, skill(skill)
, target(target)
, attacker(attacker)
{
	setTexture(skill->getTexture());
	getAnimation()->set(eAnimIdle, eDirDown);
	getAnimation()->setLoopType(eAnimLoopOnce);
	getAnimation()->start();
}
SkillInstance::SkillInstance(ObjectManager *manager)
: SkillInstance(manager, 0, 0, 0)
{

}
SkillInstance::~SkillInstance()
{

}

void SkillInstance::setSkill(Skill *skill)
{
	this->skill = skill;
}
Skill *SkillInstance::getSkill()
{
	return skill;
}
void SkillInstance::setTarget(NpcInstance *target)
{
	this->target = target;
}
NpcInstance *SkillInstance::getTarget()
{
	return target;
}

void SkillInstance::onUpdateSkillShoot(float dt)
{
	glm::vec2 dir = glm::normalize(target->getPosition() - getPosition());
	float dist = glm::length(target->getPosition() - getPosition());

	if (!getAnimation()->isAnimated())
	{
		if (getAnimation()->getCurrentAnimation() == EAnimations::eAnimIdle) {
			getAnimation()->set(EAnimations::eAnimWalk, getAnimation()->getCurrentDirection());
			getAnimation()->setLoopType(EAnimationLoop::eAnimLoopRepeat);
			getAnimation()->start();
		}
		else if (getAnimation()->getCurrentAnimation() == EAnimations::eAnimDie) {
			this->destroy();
		}
	}

	if (getAnimation()->getCurrentAnimation() == eAnimWalk)
	{
		if (dist > 100.0f*dt)
		{
			getAnimation()->set(getAnimation()->getCurrentAnimation(), dir);

			move(dir * 100.0f * dt);
		}
		else
		{
			getAnimation()->set(EAnimations::eAnimDie, getAnimation()->getCurrentDirection());
			getAnimation()->setLoopType(EAnimationLoop::eAnimLoopOnce);
			target->makeDamage(skill->getStatistics());
		}
	}
}
void SkillInstance::onUpdateSkillHit(float dt)
{
	glm::vec2 dir = glm::normalize(target->getPosition() - attacker->getPosition());
	float dist = glm::length(target->getPosition() - getPosition());

	if (!getAnimation()->isAnimated())
	{
		if (getAnimation()->getCurrentAnimation() == EAnimations::eAnimIdle) {
			getAnimation()->set(EAnimations::eAnimDie, getAnimation()->getCurrentDirection());
			getAnimation()->setLoopType(EAnimationLoop::eAnimLoopOnce);
			getAnimation()->start();
			target->makeDamage(skill->getStatistics());
		}
		else if (getAnimation()->getCurrentAnimation() == EAnimations::eAnimDie) {
			this->destroy();
		}
	}

	if (dist > 100.0f*dt)
	{
		getAnimation()->set(getAnimation()->getCurrentAnimation(), dir);
	}
}
void SkillInstance::onUpdateSkillHitShoot(float dt)
{

}
void SkillInstance::onUpdate(float dt)
{
	super::onUpdate(dt);

	switch (skill->getType())
	{
	case ESkillType::eSkillShoot: onUpdateSkillShoot(dt); break;
	case ESkillType::eSkillHit: onUpdateSkillHit(dt); break;
	case ESkillType::eSkillHitShoot: onUpdateSkillHitShoot(dt); break;
	}
}