#include "map.h"

#include "Game.h"

Map::Map() :
groundData(0),
width(0),
height(0),
tileset(0)
{

}
Map::~Map()
{
	delete groundData;
}

void Map::setTileset(Texture *tileset)
{
	this->tileset = tileset;
}
void Map::load(const char *path)
{
	FILE *file;
	int size[2];

	fopen_s(&file, path, "rb");
	fread(&size, sizeof(int), 2, file);
	groundData = new short[size[0] * size[1]];
	fread(&groundData[0], sizeof(short), size[0]*size[1], file);
	fclose(file);

	width = size[0];
	height = size[1];
}

void Map::onDraw()
{
	loop(y, height)
	{
		loop(x, width)
		{
			int dataId = y*width + x;
			game.getRenderer()->render(
				tileset->texture,
				vec2(groundData[dataId] % TILES_NUM_ROWS * TILE_SIZE,
				groundData[dataId] / TILES_NUM_ROWS * TILE_SIZE),
				vec2(TILE_SIZE,
				TILE_SIZE),
				vec2(x*TILE_SIZE,
				y*TILE_SIZE),
				vec2(TILE_SIZE,
				TILE_SIZE)
				);
		}
	}
}

void Map::createTemplate(int w, int h)
{
	groundData = new short[w * h];
	width = w;
	height = h;
	loop(y, height - 2)
	{
		loop(x, width)
		{
			if (x == 0 && y == 0) groundData[y * width + x] = 0;
			else if (x == 0 && y == height - 3) groundData[y * width + x] = TILES_NUM_ROWS * 4;
			else if (x == width - 1 && y == 0) groundData[y * width + x] = 5;
			else if (x == width - 1 && y == height - 3) groundData[y * width + x] = TILES_NUM_ROWS * 4 + 5;
			else if (x == 0) groundData[y * width + x] = TILES_NUM_ROWS;
			else if (x == width - 1) groundData[y * width + x] = TILES_NUM_ROWS + 5;
			else if (y == 0) groundData[y * width + x] = 4;
			else if (y == height - 3) groundData[y * width + x] = TILES_NUM_ROWS * 4 + 4;
			else groundData[y * width + x] = TILES_NUM_ROWS + 4;
		}
	}
	groundData[(h - 2) * width] = TILES_NUM_ROWS * 6;
	groundData[(h - 1) * width] = TILES_NUM_ROWS * 7;
	groundData[(h - 2) * width + width - 1] = TILES_NUM_ROWS * 6 + 4;
	groundData[(h - 1) * width + width - 1] = TILES_NUM_ROWS * 7 + 4;
	loop(x, width - 2)
	{
		groundData[(h - 2) * width + x + 1] = TILES_NUM_ROWS * 6 + 1;
		groundData[(h - 1) * width + x + 1] = TILES_NUM_ROWS * 7 + 1;
	}
}