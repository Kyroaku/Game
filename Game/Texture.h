#pragma once

#include <map>

#include <GuiRenderer.h>
#include "Macros.h"

#include "Animation.h"

class Texture
{
public:
	GuiTexture *texture;
	glm::ivec2 segments;
	std::map<int, AnimationKey>animations;

	Texture(GuiTexture *_texture, glm::ivec2 _segments = glm::ivec2(1));

	void setAnimation(int _animation, unsigned int _start, unsigned int _end);
};