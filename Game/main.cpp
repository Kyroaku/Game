#include "main.h"

int main()
{
	float deltaTime = 0.f;
	long lastFrameTime = SDL_GetTicks();
	SDL_Event event;

	game.init();

	while (game.isRunning())
	{
		deltaTime = (float)(SDL_GetTicks() - lastFrameTime) / 1000.f;
		lastFrameTime = SDL_GetTicks();

		while (SDL_PollEvent(&event))
		{
			GuiInput::get().onEvent(event);

			switch (event.type)
			{
			case SDL_QUIT:
				game.quit();
				break;
			}

			game.onInput();
		}
	
		game.onUpdate(deltaTime);
		game.onRender();

		SDL_RenderPresent(game.getSDLRenderer());
		SDL_Delay(0);
	}
	
	return 0;
}