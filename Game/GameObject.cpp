#include "GameObject.h"

#include "Game.h"

GameObject::GameObject(ObjectManager *manager) :
GameObject(manager, 0, glm::vec2(0.0f), glm::vec2(0.0f))
{

}
GameObject::GameObject(ObjectManager *manager, Texture *texture, glm::vec2 pos, glm::vec2 size) :
texture(texture),
pos(pos),
size(size),
destroyed(false)
{
	manager->addObject(this);
	animation = new Animation();
}
GameObject::~GameObject()
{

}
void GameObject::setTexture(Texture *t)
{
	texture = t;
	animation->setKeys(&t->animations);
}

void GameObject::setPosition(glm::vec2 p)
{
	this->pos = p;
}
void GameObject::setPosition(float x, float y)
{
	this->pos = glm::vec2(x, y);
}
void GameObject::setSize(glm::vec2 s)
{
	this->size = s;
}
void GameObject::setSize(float w, float h)
{
	this->size = glm::vec2(w, h);
}
Texture *GameObject::getTexture()
{
	return texture;
}
glm::vec2 GameObject::getPosition()
{
	return pos;
}
glm::vec2 GameObject::getSize()
{
	return size;
}
Animation *GameObject::getAnimation()
{
	return animation;
}
bool GameObject::isAnimated()
{
	return animation->isAnimated();
}
bool GameObject::hasAnimations()
{
	return animation->hasAnimations();
}
void GameObject::move(glm::vec2 delta)
{
	pos += delta;
}
void GameObject::destroy()
{
	destroyed = true;
}
bool GameObject::isDestroyed()
{
	return destroyed;
}

void GameObject::onInput()
{

}
void GameObject::onUpdate(float dt)
{
	getAnimation()->onUpdate(dt);
}
void GameObject::onDraw()
{
#ifdef _DEBUG_SHOW_GAMEOBJECT_SIZE
	SDL_Rect rect;
	rect.x = getPosition().x - getSize().x/2.0f;
	rect.y = getPosition().y - getSize().y;
	rect.w = getSize().x;
	rect.h = getSize().y;
	SDL_RenderDrawRect(game.getRenderer(), &rect);
#endif

	if (hasAnimations())
	{
		int texWidth, texHeight;
		SDL_QueryTexture((SDL_Texture*)getTexture()->texture, 0, 0, &texWidth, &texHeight);

		game.getRenderer()->render(
			getTexture()->texture,
			vec2((getAnimation()->getCurrentFrame() % texture->segments.x) * texWidth / texture->segments.x,
			(getAnimation()->getCurrentFrame() / texture->segments.x) * texHeight / texture->segments.y),
			ivec2(texWidth, texHeight) / texture->segments,
			vec2(getPosition().x - getSize().x / 2, getPosition().y - getSize().y),
			getSize()
			);
	}
	else
	{
		game.getRenderer()->render(
			getTexture()->texture,
			vec2(0.0f),
			vec2(-1.0f),
			vec2(getPosition().x - getSize().x / 2, getPosition().y - getSize().y),
			getSize()
			);
	}
}

bool GameObject::minFunc(GameObject *go1, GameObject *go2)
{
	if (go1->getAnimation()->getCurrentAnimation() != go2->getAnimation()->getCurrentAnimation() ||
		go1->isAnimated() != go2->isAnimated())
	{
		if (go1->getAnimation()->getCurrentAnimation() == eAnimDie && !go1->isAnimated())
			return true;
		else
		if (go2->getAnimation()->getCurrentAnimation() == eAnimDie && !go2->isAnimated())
			return false;
	}
	
	return go1->getPosition().y < go2->getPosition().y;
}