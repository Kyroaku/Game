#include "Skill.h"

Skill::Skill(ESkillType type, Texture *texture, EAnimations castAnimation)
: type(type)
, texture(texture)
, castAnimation(castAnimation)
{

}
Skill::Skill()
: Skill(ESkillType::eSkillShoot, 0, eAnimSkill1)
{

}
Skill::~Skill()
{

}

void Skill::setType(ESkillType type)
{
	this->type = type;
}
ESkillType Skill::getType()
{
	return type;
}
void Skill::setTexture(Texture *texture)
{
	this->texture = texture;
}
Texture *Skill::getTexture()
{
	return texture;
}
Statistics &Skill::getStatistics()
{
	return statistics;
}
void Skill::setCastAnimation(EAnimations castAnimation)
{
	this->castAnimation = castAnimation;
}
EAnimations Skill::getCastAnimation()
{
	return castAnimation;
}
void Skill::setPositionOffset(EDirection dir, glm::vec2 p)
{
	positionOffset[(int)dir] = p;
}
void Skill::setPositionOffset(glm::vec2 up, glm::vec2 right, glm::vec2 down, glm::vec2 left)
{
	positionOffset[EDirection::eDirUp] = up;
	positionOffset[EDirection::eDirRight] = right;
	positionOffset[EDirection::eDirDown] = down;
	positionOffset[EDirection::eDirLeft] = left;
}
glm::vec2 Skill::getPositionOffset(EDirection dir)
{
	return positionOffset[(int)dir];
}
void Skill::setSize(glm::vec2 s)
{
	size = s;
}
glm::vec2 Skill::getSize()
{
	return size;
}