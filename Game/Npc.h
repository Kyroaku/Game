#pragma once

#include <vector>

#include "GameObject.h"
#include "GuiText.h"
#include "Statistics.h"
#include "DropItem.h"

enum class ENpcTypes
{
	eNpcMonster,
	eNpcMerchant,
	eNpcPlayer,
};

enum EClothes
{
	eClothBreastplate = 0,
	eClothPants,
	eClothHelmet,
	eClothGloves,
	eClothShoes,
	eClothLast
};

enum ENpcSex
{
	eSexMale = 0,
	eSexFemale = 1
};

class Npc
{
	Texture *texture[2]; 
	std::string name;
	ENpcTypes type;
	ENpcSex sex;
	int clothes[EClothes::eClothLast];
	std::string dialog; /* merchant only */
	std::vector<DropItem>drop;
	Statistics statistics;

public:
	Npc();
	Npc(std::string name, ENpcTypes type);
	virtual ~Npc();

	virtual void setTexture(ENpcSex sex, Texture *texture);
	virtual Texture *getTexture();
	virtual void setName(std::string name);
	virtual std::string getName();
	virtual void setType(ENpcTypes type);
	virtual ENpcTypes getType();
	virtual void setSex(ENpcSex sex);
	virtual ENpcSex getSex();
	virtual bool isTypeOf(ENpcTypes type);
	virtual void setCloth(int part, int id);
	virtual int getCloth(int part);
	virtual void setDialog(std::string dialog);
	virtual std::string getDialogPath();
	virtual std::vector<DropItem> &getDropList();
	virtual Statistics &getStatistics();
};