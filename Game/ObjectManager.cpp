#include "ObjectManager.h"

#include "Game.h"

ObjectManager::ObjectManager()
{

}
ObjectManager::~ObjectManager()
{
	loopu(i, objects.size())
	{
		if (objects.at(i))
		{
			if (objects.at(i))
				delete objects.at(i);
			objects.erase(objects.begin() + i);
		}
	}
}
void ObjectManager::addObject(GameObject *go)
{
	objects.push_back(go);
}
GameObject *ObjectManager::getObject(int id)
{
	return objects.at(id);
}
void ObjectManager::onInput()
{
	loopu(i, objects.size())
	{
		objects[i]->onInput();
	}
}
void ObjectManager::onUpdate(float dt)
{
	if (!objects.empty())
		std::sort(&objects[0], &objects[0] + objects.size(), GameObject::minFunc);
	loopu(i, objects.size())
	{
		if (objects[i]->isDestroyed()) {
			delete objects[i];
			objects.erase(objects.begin() + i);
			i--;
			continue;
		}
		objects[i]->onUpdate(dt);
	}
}
void ObjectManager::onDraw()
{
	loopu(i, objects.size())
	{
		objects[i]->onDraw();
	}
}