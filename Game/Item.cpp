#include "Item.h"

Item::Item(std::string name, EItemType type, Texture *icon)
: name(name)
, type(type)
, icon(icon)
{
	texture[0] = texture[1] = 0;
}
Item::Item()
: Item("Unnamed", EItemType::eItem, 0)
{

}
Item::~Item()
{

}

void Item::setName(std::string name)
{
	this->name = name;
}
std::string Item::getName()
{
	return name;
}
void Item::setType(EItemType type)
{
	this->type = type;
}
EItemType Item::getType()
{
	return type;
}
void Item::setIcon(Texture *icon)
{
	this->icon = icon;
}
Texture *Item::getIcon()
{
	return icon;
}
void Item::setTextureMale(Texture *texture)
{
	this->texture[0] = texture;
}
Texture *Item::getTextureMale()
{
	return texture[0];
}
void Item::setTextureFemale(Texture *texture)
{
	this->texture[1] = texture;
}
Texture *Item::getTextureFemale()
{
	return texture[1];
}
void Item::setTexture(Texture *texture, int sex)
{
	this->texture[sex] = texture;
}
Texture *Item::getTexture(int sex)
{
	return texture[sex];
}