#include "Console.h"

#include "Game.h"

#include <thread>
#include <chrono>

void Console::AdminMenu()
{
	printf("============= Admin menu:\n[1] Npc\n");

	switch (_getch())
	{
	case '1':
		NpcMenu();
		break;

	default:
		printf("Wrong id!\n");
		return;
		break;
	}
}

void Console::NpcMenu()
{
	int npc_id;
	printf("============= Select NPC:\n");
	printf("[-1] Player\n");
	for (int i = 0; i < (int)game.npcs.size(); i++)
		printf("[%d] %s\n", i, game.npcs[i]->getName().c_str());
	scanf_s("%d", &npc_id);
	if (npc_id < -1 || npc_id > (int)game.npcs.size()) {
		printf("%d - Wrong id!\n", npc_id);
		return;
	}

	printf("============= Npc menu:\n[1] Clothes\n[2] Sex\n");
	int asd;
	switch (asd = _getch())
	{
	case '1':
		ClothesMenu(npc_id);
		break;

	case '2':
		SexMenu(npc_id);
		break;

	default:
		printf("%d - Wrong id!\n", asd);
		return;
		break;
	}
}

void Console::ClothesMenu(int npc_id)
{
	EClothes cloth_type;
	int item_id;

	printf("============= Select cloth:\n");
	printf("[1] Breastplate\n[2] Pants\n[3] Helmet\n[4] Gloves\n[5] Shoes\n");
	switch (_getch())
	{
	case '1': cloth_type = eClothBreastplate; break;
	case '2': cloth_type = eClothPants; break;
	case '3': cloth_type = eClothHelmet; break;
	case '4': cloth_type = eClothGloves; break;
	case '5': cloth_type = eClothShoes; break;
	default:
		printf("Wrong id!\n");
		return;
		break;
	}

	printf("============= Select Item:\n");
	for (int i = 0; i < (int)game.items.size(); i++)
		printf("[%d] %s\n", i, game.items[i]->getName().c_str());
	
	scanf_s("%d", &item_id);
	if (item_id < 0 || item_id > (int)game.items.size()) {
		printf("Wrong id!\n");
		return;
	}

	if (npc_id >= 0)
		game.npcs[npc_id]->setCloth(cloth_type, item_id);
	else
		game.player->getNpc()->setCloth(cloth_type, item_id);
}

void Console::SexMenu(int npc_id)
{
	ENpcSex sex;

	printf("============= Select sex:\n[1] Male\n[2] Female\n");
	switch (_getch())
	{
	case '1': sex = eSexMale; break;
	case '2': sex = eSexFemale; break;
	default:
		printf("Wrong id!\n");
		return;
		break;
	}

	if (npc_id >= 0)
		game.npcs[npc_id]->setSex(sex);
	else
		game.player->getNpc()->setSex(sex);
}