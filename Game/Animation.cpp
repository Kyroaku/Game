#include "Animation.h"

AnimationKey::AnimationKey() :
startFrame(0),
endFrame(0)
{

}
AnimationKey::AnimationKey(unsigned int start, unsigned int end) :
startFrame(start),
endFrame(end)
{

}
void AnimationKey::set(unsigned int start, unsigned int end)
{
	startFrame = start;
	endFrame = end;
}

Animation::Animation() :
speed(1.0f),
timer(0.f),
frame(0),
animation(eAnimIdle),
direction(eDirDown),
keys(0),
loopType(EAnimationLoop::eAnimLoopRepeat),
animated(false)
{

}
void Animation::setSpeed(float s)
{
	this->speed = s;
}
void Animation::setFrame(int i)
{
	if (!keys)
		return;

	this->frame = i + keys->at(animation+direction).startFrame;
}
void Animation::setKeys(std::map<int, AnimationKey> *keys)
{
	this->keys = keys;
}
void Animation::setLoopType(EAnimationLoop type)
{
	loopType = type;
}
void Animation::set(EAnimations anim, EDirection direction, float speed, EAnimationLoop loop)
{
	if (speed > 0.0f)
		this->speed = speed;

	if (loop != EAnimationLoop::eAnimLoopNone)
		this->loopType = loop;

	if (getCurrentAnimation() + getCurrentDirection() != anim + direction)
	{
		this->animation = anim;
		this->direction = direction;
		setFrame(0);
		this->timer = 0.f;
	}
}
void Animation::set(EAnimations anim, glm::vec2 dir, float speed, EAnimationLoop loop)
{
	if (abs(dir.x) > abs(dir.y)) {
		if (dir.x < 0.f)	set(anim, eDirLeft, speed, loop);
		else				set(anim, eDirRight, speed, loop);
	}
	else {
		if (dir.y < 0.f)	set(anim, eDirUp, speed, loop);
		else				set(anim, eDirDown, speed, loop);
	}
}
float Animation::getSpeed()
{
	return speed;
}
int Animation::getCurrentFrame()
{
	return frame;
}
EAnimations Animation::getCurrentAnimation()
{
	return animation;
}
EDirection Animation::getCurrentDirection()
{
	return direction;
}
std::map<int, AnimationKey> *Animation::getKeys()
{
	return keys;
}
EAnimationLoop Animation::getLoopType()
{
	return loopType;
}
void Animation::start()
{
	animated = true;
}
void Animation::stop()
{
	animated = false;
}
bool Animation::isAnimated()
{
	return animated;
}
bool Animation::hasAnimations()
{
	return keys == 0 ? false : true;
}
void Animation::onUpdate(float dt)
{
	if (!animated)
		return;

	timer += dt;

	float speedPerAnimation = 1.0f / speed / (keys->at(animation + direction).endFrame - keys->at(animation + direction).startFrame + 1);
	if (timer >= speedPerAnimation)
	{
		timer -= speedPerAnimation;
		frame++;
	}
	if (timer <= -speedPerAnimation)
	{
		timer += speedPerAnimation;
		frame--;
	}


	if (frame > keys->at(animation + direction).endFrame) {
		switch (getLoopType())
		{
		case EAnimationLoop::eAnimLoopRepeat:
			frame = keys->at(animation + direction).startFrame;
			break;

		case EAnimationLoop::eAnimLoopOnce:
			frame = keys->at(animation + direction).endFrame;
			this->stop();
			break;

		default:
			break;
		}
	}

	if (frame < keys->at(animation + direction).startFrame) {
		frame = keys->at(animation + direction).endFrame;
	}
}