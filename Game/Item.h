#pragma once

#include "Texture.h"

enum EItemType
{
	eItem,
	eItemWeapon,
	eItemBreastplate,
	eItemPants,
	eItemHelmet,
	eItemGloves,
	eItemShoes
};

class Item
{
	std::string name;
	EItemType type;
	Texture *icon;
	Texture *texture[2];

public:
	Item();
	Item(std::string name, EItemType type, Texture *icon);
	~Item();

	void setName(std::string name);
	std::string getName();
	void setType(EItemType type);
	EItemType getType();
	void setIcon(Texture *icon);
	Texture *getIcon();
	void setTextureMale(Texture *texture);
	Texture *getTextureMale();
	void setTextureFemale(Texture *texture);
	Texture *getTextureFemale();
	void setTexture(Texture *texture, int sex);
	Texture *getTexture(int sex);
};