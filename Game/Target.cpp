#include "Target.h"

Target::Target() :
npc(0),
action(ETargetAction::eNone)
{

}
void Target::set(NpcInstance *npc, ETargetAction action)
{
	this->npc = npc;
	this->action = action;
}
void Target::setAction(ETargetAction action)
{
	this->action = action;
}
NpcInstance *Target::getNpc()
{
	return npc;
}
ETargetAction Target::getAction()
{
	return action;
}
bool Target::isFollowed()
{
	return (
		action != ETargetAction::eNone
		);
}