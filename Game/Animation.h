#pragma once

#include <map>
#include <glm\glm.hpp>

#include "Macros.h"

enum EAnimationLoop
{
	eAnimLoopNone,
	eAnimLoopOnce,
	eAnimLoopRepeat
};

enum EAnimations
{
	eAnimWalk = 0,
	eAnimIdle = 4,
	eAnimDie = 8,
	eAnimSkill1 = 12
};

enum EDirection
{
	eDirUp = 0,
	eDirRight = 1,
	eDirDown = 2,
	eDirLeft = 3
};

class AnimationKey
{
public:
	unsigned int startFrame, endFrame;

	AnimationKey();
	AnimationKey(unsigned int start, unsigned int end);
	void set(unsigned int start, unsigned int end);
};

class Animation
{
	float speed, timer;
	unsigned int frame;
	EAnimations animation;
	EDirection direction;
	std::map<int, AnimationKey>*keys;
	EAnimationLoop loopType;
	bool animated;

public:
	Animation();
	void setSpeed(float s);
	void setFrame(int i);
	void setKeys(std::map<int, AnimationKey> *keys);
	void setLoopType(EAnimationLoop type);
	void set(EAnimations anim, EDirection direction, float speed = -1.0f, EAnimationLoop loop = EAnimationLoop::eAnimLoopNone);
	void set(EAnimations anim, glm::vec2 dir, float speed = -1.0f, EAnimationLoop loop = EAnimationLoop::eAnimLoopNone);
	float getSpeed();
	int getCurrentFrame();
	EAnimations getCurrentAnimation();
	EDirection getCurrentDirection();
	std::map<int, AnimationKey> *getKeys();
	EAnimationLoop getLoopType();
	void start();
	void stop();
	bool isAnimated();
	bool hasAnimations();
	void onUpdate(float dt);
};