#pragma once

#define NUM_ANIMATIONS			4
#define NUM_HEROES_TEXTURES		2
#define TILES_NUM_ROWS			9
#define TILE_SIZE				16

#define loop(i, n)				for(int i = 0; i < n; i++)
#define loopu(i, n)				for(unsigned int i = 0; i < n; i++)