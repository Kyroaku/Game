#pragma once

#include "NpcInstance.h"
#include "Skill.h"

class NpcInstance;

enum class ETargetAction
{
	eNone,
	eGo,
	eTalk,
	eAttack
};

class Target
{
	NpcInstance *npc;
	ETargetAction action;
	Skill *skill;

public:
	Target();
	void set(NpcInstance *npc, ETargetAction action = ETargetAction::eNone);
	void setAction(ETargetAction action);
	NpcInstance *getNpc();
	ETargetAction getAction();
	bool isFollowed();
};