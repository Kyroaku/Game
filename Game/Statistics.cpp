#include "Statistics.h"

Statistics::Statistics(float hp, float strength, float stamina, float intelligence, float mentality, float agility)
: hp(hp)
, strength(strength)
, stamina(stamina)
, intelligence(intelligence)
, mentality(mentality)
, agility(agility)
{

}
Statistics::Statistics()
: Statistics(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f)
{

}
void Statistics::set(float hp, float strength, float stamina, float intelligence, float mentality, float agility)
{
	this->hp = hp;
	this->strength = strength;
	this->stamina = stamina;
	this->intelligence = intelligence;
	this->mentality = mentality;
	this->agility = agility;
}