#pragma once

#include <SDL/SDL.h>
#include <glm\glm.hpp>
#include <vector>

#include "Macros.h"
#include "Assets.h"
#include "Animation.h"
#include "ObjectManager.h"

class ObjectManager;

class GameObject
{
	Texture *texture;
	glm::vec2 pos, size;
	Animation *animation;
	bool destroyed;

public:
	GameObject(ObjectManager *manager);
	GameObject(ObjectManager *manager, Texture *texture, glm::vec2 pos, glm::vec2 size);
	virtual ~GameObject();
	virtual void setTexture(Texture *t);
	virtual void setPosition(glm::vec2 p);
	virtual void setPosition(float x, float y);
	virtual void setSize(glm::vec2 s);
	virtual void setSize(float w, float h);
	virtual Texture *getTexture();
	virtual glm::vec2 getPosition();
	virtual glm::vec2 getSize();
	virtual Animation *getAnimation();
	virtual bool isAnimated();
	virtual bool hasAnimations();
	virtual void move(glm::vec2 delta);
	virtual void destroy();
	virtual bool isDestroyed();

	virtual void onInput();
	virtual void onUpdate(float dt);
	virtual void onDraw();

	static bool minFunc(GameObject *go1, GameObject *go2);
};