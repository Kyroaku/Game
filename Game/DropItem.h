#pragma once

#include <stdlib.h>

class DropItem
{
public:
	int min, max, id;

	DropItem(int id, int min, int max);
	DropItem();

	int getQuantity();
};