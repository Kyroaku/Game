#include "DropItem.h"

DropItem::DropItem(int id, int min, int max)
: id(id)
, min(min)
, max(max)
{

}
DropItem::DropItem()
: DropItem(0, 0, 0)
{

}
int DropItem::getQuantity()
{
	return rand() % (max - min) + min;
}