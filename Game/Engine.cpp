#include "Engine.h"

Engine::Engine() :
renderer(0)
{
	SDL_Init(SDL_INIT_EVERYTHING);
	sdlWindow = SDL_CreateWindow("Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);
	sdlRenderer = SDL_CreateRenderer(sdlWindow, 0, SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 0, 255);
	renderer = new GuiRendererSDL(sdlWindow, sdlRenderer);

	assets = new Assets(renderer);
}
Engine::~Engine()
{
	delete assets;

	delete renderer;
	SDL_Quit();
}
GuiRenderer *Engine::getRenderer()
{
	return renderer;
}
SDL_Window *Engine::getSDLWindow()
{
	return sdlWindow;
}
SDL_Renderer *Engine::getSDLRenderer()
{
	return sdlRenderer;
}

glm::vec2 Engine::getTextureSize(SDL_Texture *texture)
{
	int w, h;
	SDL_QueryTexture(texture, 0, 0, &w, &h);
	return glm::vec2(w, h);
}

bool Engine::isPointInRect(glm::vec2 p, glm::vec2 r1, glm::vec2 r2)
{
	if (p.x < r1.x || p.x > r1.x + r2.x || p.y < r1.y || p.y > r1.y + r2.y)
		return false;
	return true;
}